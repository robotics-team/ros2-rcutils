ros2-rcutils (6.9.4-1) unstable; urgency=medium

  * New upstream version 6.9.4
  * Refresh patches (no functional changes)

 -- Timo Röhling <roehling@debian.org>  Sun, 16 Feb 2025 14:07:38 +0100

ros2-rcutils (6.9.3-1) unstable; urgency=medium

  * Update d/watch to latest GitHub recipe
  * New upstream version 6.9.3

 -- Timo Röhling <roehling@debian.org>  Thu, 10 Oct 2024 15:02:59 +0200

ros2-rcutils (6.9.2-1) unstable; urgency=medium

  * New upstream version 6.9.2
  * Wrap and sort Debian package files
  * New rcutils_set_env_overwrite symbol

 -- Timo Röhling <roehling@debian.org>  Wed, 25 Sep 2024 17:48:55 +0200

ros2-rcutils (6.9.1-1) unstable; urgency=medium

  * New upstream version 6.9.1

 -- Timo Röhling <roehling@debian.org>  Tue, 03 Sep 2024 16:43:29 +0200

ros2-rcutils (6.9.0-1) unstable; urgency=medium

  * New upstream version 6.9.0
  * Refresh patches (no functional changes)

 -- Timo Röhling <roehling@debian.org>  Fri, 12 Jul 2024 18:17:12 +0200

ros2-rcutils (6.8.0-1) unstable; urgency=medium

  * New upstream version 6.8.0
  * Refresh patches (no functional changes)
  * Update d/copyright

 -- Timo Röhling <roehling@debian.org>  Sun, 28 Apr 2024 23:51:12 +0200

ros2-rcutils (6.7.0-1) unstable; urgency=medium

  * New upstream version 6.7.0

 -- Timo Röhling <roehling@debian.org>  Sat, 20 Apr 2024 00:01:29 +0200

ros2-rcutils (6.6.0-1) unstable; urgency=medium

  * New upstream version 6.6.0
  * Refresh patches (no functional changes)
  * Bump Standards-Version to 4.7.0
  * Update d/symbols

 -- Timo Röhling <roehling@debian.org>  Wed, 10 Apr 2024 22:13:41 +0200

ros2-rcutils (6.5.0-1) unstable; urgency=medium

  * New upstream version 6.5.0
  * Refresh patches (no functional changes)

 -- Timo Röhling <roehling@debian.org>  Thu, 04 Jan 2024 12:46:33 +0100

ros2-rcutils (6.4.1-1) unstable; urgency=medium

  * New upstream version 6.4.1

 -- Timo Röhling <roehling@debian.org>  Sun, 12 Nov 2023 22:04:35 +0100

ros2-rcutils (6.4.0-1) unstable; urgency=medium

  * New upstream version 6.4.0

 -- Timo Röhling <roehling@debian.org>  Thu, 26 Oct 2023 11:41:06 +0200

ros2-rcutils (6.3.1-1) unstable; urgency=medium

  * New upstream version 6.3.1

 -- Timo Röhling <roehling@debian.org>  Thu, 14 Sep 2023 14:15:22 +0200

ros2-rcutils (6.3.0-1) unstable; urgency=medium

  * New upstream version 6.3.0
  * Refresh patches
  * Update symbols
  * Bump Standards-Version to 4.6.2

 -- Timo Röhling <roehling@debian.org>  Tue, 27 Jun 2023 23:24:19 +0200

ros2-rcutils (6.0.1-1) unstable; urgency=medium

  * New upstream version 6.0.1

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 03 Nov 2022 17:37:30 +0100

ros2-rcutils (6.0.0-2) unstable; urgency=medium

  * Upload to unstable.
  * Switch to Github API for d/watch

 -- Timo Röhling <roehling@debian.org>  Sun, 02 Oct 2022 16:02:32 +0200

ros2-rcutils (6.0.0-1) experimental; urgency=medium

  * New upstream version 6.0.0
  * Bump SOVERSION to 1d
  * Enable hardening flags

 -- Timo Röhling <roehling@debian.org>  Sat, 17 Sep 2022 21:00:48 +0200

ros2-rcutils (5.2.0-7) unstable; urgency=medium

  * Source only upload
  * Remove old d/README.source
  * Fix date in d/copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 17 Jun 2022 15:17:30 +0200

ros2-rcutils (5.2.0-6) unstable; urgency=medium

  * Extend d/copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 15 Jun 2022 08:50:51 +0200

ros2-rcutils (5.2.0-5) unstable; urgency=medium

  * Add python3-rcutils package

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 13 Jun 2022 22:14:45 +0200

ros2-rcutils (5.2.0-4) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Jochen Sprickerhof ]
  * Add patch for ROS 1 compatibility
  * Bump policy version (no changes)
  * Drop versioned dependencies

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 02 Jun 2022 21:48:06 +0200

ros2-rcutils (5.2.0-3) unstable; urgency=medium

  * Move libatomic test out of BUILD_TESTING conditional block

 -- Timo Röhling <roehling@debian.org>  Mon, 16 May 2022 11:41:08 +0200

ros2-rcutils (5.2.0-2) unstable; urgency=medium

  * Stop generating cruft files

 -- Timo Röhling <roehling@debian.org>  Sun, 15 May 2022 16:19:54 +0200

ros2-rcutils (5.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #1010779)

 -- Timo Röhling <roehling@debian.org>  Tue, 10 May 2022 15:56:02 +0200
